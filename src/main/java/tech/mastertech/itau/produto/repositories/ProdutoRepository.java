package tech.mastertech.itau.produto.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.models.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
	public Iterable<Produto> findAllByCategoria(Categoria categoria);
}
